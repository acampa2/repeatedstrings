import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Main {

    // Complete the repeatedString function below.
    static long repeatedString(String s, long n) {
        long numberOfReps = n/s.length();
        long remainder = n%s.length();
        long total = 0;
        for(int a = 0; a < s.length(); a++){
            if(s.charAt(a) == 'a'){
                total++;
            }
        }
        total = total * numberOfReps;
        for(int a = 0; a < remainder; a++){
            if(s.charAt(a) == 'a'){
                total++;
            }
        }
        return total;
    }


    public static void main(String[] args) throws IOException {
    String s = "aba";
    long n = 10;

        long result = repeatedString(s, n);

        System.out.println(result);
    }
}
